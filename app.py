from datetime import datetime

from flask import Flask, render_template, request, redirect, session
from flask_sqlalchemy import SQLAlchemy
from geopy import Yandex
from geopy.distance import geodesic
import folium
from sqlalchemy.util import NoneType

from utils import get_center_coor, get_zoom, frange

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///map.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# DB model for saving input locations data
class DisData(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    location = db.Column(db.String(200))
    destination = db.Column(db.String(200))
    distance = db.Column(db.Numeric(10, 2))
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'Distance from {self.location} to {self.destination} is {self.distance}'


# @app.route('/')
# def index():
#     return "Hello Maka"


@app.route('/destination')
def destination():
    destinations = DisData.query.order_by(DisData.id.desc()).all()
    return render_template("list.html", destinations=destinations)


@app.route('/', methods=['POST', 'GET'])
def create():
    geolocator = Yandex(api_key='ec25a04c-21dd-4428-980a-dbfe9c6b1718')
    if request.method == 'POST':
        location = 'MKAD,Moscow'
        location_map = geolocator.geocode(location)
        destination = request.form['destination']
        destination_map = geolocator.geocode(destination)
        if destination_map:
            d_lat = destination_map.latitude
            d_lon = destination_map.longitude
            l_lat = location_map.latitude
            l_lon = location_map.longitude
            pointA = (l_lat, l_lon)
            print(pointA)
            pointB = (d_lat, d_lon)
            print(pointB)
            max_lat = 55.898947
            min_lat = 55.58786  #Biryulovo

            if d_lat not in frange(min_lat, max_lat):
                distance = round(geodesic(pointA, pointB).km, 2)
                des = DisData(location=location, destination=destination, distance=distance)
                try:
                    db.session.add(des)
                    db.session.commit()
                    return redirect('/distance')
                except:
                    return "Issue with adding the location data"
            else:
                des = DisData(location=location, destination=destination, distance=0)
                try:
                    db.session.add(des)
                    db.session.commit()
                    return redirect('/distance')
                except:
                    return "Issue with adding the location data"
        else:
            return render_template('create.html')
    else:
        return render_template('create.html')


@app.route('/destination/<int:id>')
def des_distance(id):
    geolocator = Yandex(api_key='ec25a04c-21dd-4428-980a-dbfe9c6b1718')
    destination = DisData.query.get(id)
    location = destination.location
    location_map = geolocator.geocode(location)
    destination_dir = destination.destination
    destination_map = geolocator.geocode(destination_dir)
    d_lat = destination_map.latitude
    d_lon = destination_map.longitude
    l_lat = location_map.latitude
    l_lon = location_map.longitude

    m_point = folium.Map(width=800, height=400, location=get_center_coor(l_lat, l_lon, d_lat, d_lon), zoom_start=get_zoom(destination.distance))
    folium.Marker([l_lat, l_lon], tooltip='click here', popup=location, icon=folium.Icon(color='purple')).add_to(m_point)
    folium.Marker([d_lat, d_lon], tooltip='click here', popup=destination_dir, icon=folium.Icon(color='blue')).add_to(
        m_point)
    m_point = m_point._repr_html_()
    return render_template("distance.html", destination=destination, map=m_point)


@app.route('/destination/<int:id>/del')
def delete(id):
    destination = DisData.query.get_or_404(id)

    try:
        db.session.delete(destination)
        db.session.commit()
        return redirect('/destination')
    except:
        return "Issue with removing the location data"


@app.route('/distance')
def distance_det():
    geolocator = Yandex(api_key='ec25a04c-21dd-4428-980a-dbfe9c6b1718')
    destination = DisData.query.order_by(DisData.id.desc()).first()
    location = destination.location
    location_map = geolocator.geocode(location)
    destination_dir = destination.destination
    destination_map = geolocator.geocode(destination_dir)
    d_lat = destination_map.latitude
    d_lon = destination_map.longitude
    l_lat = location_map.latitude
    l_lon = location_map.longitude

    m_point = folium.Map(width=800, height=400, location=get_center_coor(l_lat, l_lon, d_lat, d_lon),
                         zoom_start=get_zoom(destination.distance))
    folium.Marker([l_lat, l_lon], tooltip='click here', popup=location, icon=folium.Icon(color='purple')).add_to(
        m_point)
    folium.Marker([d_lat, d_lon], tooltip='click here', popup=destination_dir, icon=folium.Icon(color='blue')).add_to(
        m_point)
    m_point = m_point._repr_html_()
    return render_template("distance_det.html", destination=destination,map=m_point)


if __name__ == '__main__':
    app.run()
