Map application.
Application created for calculation of distance between MKAD, Moscow and some inputted destination.All the coordinates selected inside of MKAD are not considered during calculation and outputs 0 km.
Project is developed on Flask framework using standart templetes for rendering.
For storing the data SQLalchemy is used.
Libreries used for this application:geopy, Yandex api, geodesic, folium
Due to the small functionality of an application, the structure of the application is simple, and all the main route processing and db model is located inside off main file app.py.
On the main page ("/") we can input destination we need to be calculated, and function create will create a database, which processes information input and if it is a real address it gets coordinates of two addresses (point A and point B) using geocoder 'geopy' lib, Yandex api key. Then destination is tested whether it is not inside of MKAD using max_lat, min_lat and additional function frange (utils.py). Then with the help of 'geodesic' lib distance is calculated and finally data is saved in db. If the inputted destination is inside of MKAD  0 km distance is recorded.
After we input a destination and create function is completed we are redirected to the distance detail page ('/distance'). On this page we can see the location direction and distance, in addition to these we can see the map 'folium' and markers on it 'folium'. All the coordinates are geicoded using 'geopy' lib and Yandex api key. For better scale and zoom of map additional functions get_center_coord and get_zoom (utils.py) are used.
On the page destination list ('/destination') the list of all calculated distances is displayed.
Inside of destination list page, we can see 'Detail' button which redirects us to the destination detail page ('/destination/<int:id>') Inside of which we can see details and markers on the map.Also next to 'Detail' button there is 'Delete' button which deletes certain destination data.
